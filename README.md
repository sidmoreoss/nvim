# Neovim Configuration and Key Mappings

This repository contains my custom configuration settings and key mappings for Neovim. These settings aim to enhance the Neovim experience by adjusting various options, behaviors, and key mappings. Below is a comprehensive list of configuration settings, key mappings, and their descriptions:

## General Configuration

- `vim.g.mapleader` is set to an empty string (`""`), which changes the leader key to space for easier key mappings.

## Line Numbers and Indentation

- Line numbers are enabled using `vim.opt.nu`.

- Relative line numbers are enabled using `vim.opt.relativenumber`.

- Tabstop, soft tabstop, and shift width are set to 4 spaces for consistent code indentation.

- `vim.opt.expandtab` is set to `true`, which means spaces are used for indentation instead of tabs.

- `vim.opt.smartindent` is enabled to enable automatic smart indenting behavior.

## Text Wrapping

- Text wrapping is turned off using `vim.opt.wrap`.

## File Handling

- `vim.opt.swapfile` and `vim.opt.backup` are set to `false`, preventing the creation of swap and backup files.

- `vim.opt.undodir` is configured to store undo files in the `~/.cache/nvim/undodir` directory.

- `vim.opt.undofile` is enabled to persist undo history across sessions.

## Search and Highlighting

- Highlighting during search is turned off with `vim.opt.hlsearch`.

- Incremental search is enabled using `vim.opt.incsearch`.

## GUI and Colors

- `vim.opt.termguicolors` is set to `true` to enable true-color support in the terminal.

## Scrolling and Sign Column

- `vim.opt.scrolloff` is set to 8, providing a comfortable margin when scrolling.

- `vim.opt.signcolumn` is configured to "no" to disable the sign column.

## Filetype Settings

- `vim.opt.isfname` is appended with `@-@`, which allows "@" to be considered part of a word.

## Performance

- `vim.opt.updatetime` is set to 50 milliseconds to improve responsiveness.

## Key Mappings

- `<leader>` is remapped to space for ease of use.

- `J` and `K` in visual mode are remapped to move selected lines down and up, respectively.

- `J` in normal mode joins the current line with the one below, preserving cursor position.

- `<C-d>` and `<C-u>` in normal mode scroll the screen down and up, respectively, while keeping the cursor position.

- `n` and `N` in normal mode center the screen on the current search result.

- `<leader>p` in visual mode cuts the selected text and pastes it above the current line, essentially moving the text.

- `<leader>y` in normal or visual mode copies the selected text to the system clipboard.

- `<leader>d` in normal or visual mode deletes the selected text without affecting the system clipboard.

- `<C-c>` in insert mode acts as an escape key, allowing you to exit insert mode quickly.

- `Q` in normal mode is mapped to no operation (`<nop>`), effectively doing nothing.

- `<C-f>` in normal mode opens a new Tmux window named `tmux-sessionizer`.

- `<leader>f` in normal mode triggers the LSP (Language Server Protocol) formatting command.

- `<C-k>` and `<C-j>` in normal mode navigate through compile errors when using quickfix.

- `<leader>s` in normal mode performs a case-insensitive search and replace across the entire file.

- `<leader>x` in normal mode runs a shell command to make the current file executable.

- `<leader>vpp` in normal mode opens the Packer.nvim configuration file for editing.

- `<leader>mr` in normal mode runs the `CellularAutomaton` command to "make it rain."

- `<leader><leader>` in normal mode sources the current script, effectively reloading it.

## Usage

To use these configuration settings and key mappings, add them to your Neovim configuration file (usually `init.vim` or `init.lua`). You can customize the settings or add more as needed.

```lua
-- Example configuration in Lua

vim.g.mapleader = " "
vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true
-- ... (other settings and mappings) ...
```

```vim
" Example configuration in Vimscript

let g:mapleader = " "
set nu
set relativenumber
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
" ... (other settings and mappings) ...
```

# Plugins

## Language Server Protocol (LSP)

### LSP Preset

The `lsp-zero` plugin is used to set up LSP servers, and the "recommended" preset is applied to configure commonly used LSP servers and settings.

```lua
local lsp = require("lsp-zero")

lsp.preset("recommended")
```

### Installed LSP Servers

The `lsp.ensure_installed` function ensures that specific LSP servers (`tsserver` and `rust_analyzer`) are installed and available for your Neovim setup.

```lua
lsp.ensure_installed({
  'tsserver',
  'rust_analyzer',
})
```

### Workspace Configuration

To prevent undefined global 'vim' issues, the `lsp.nvim_workspace` function is used to ensure a proper workspace configuration.

```lua
lsp.nvim_workspace()
```

## Completion and Auto-completion (CMP)

The `cmp` plugin is utilized to enhance code completion in Neovim.

```lua
local cmp = require('cmp')
```

Custom key mappings are defined to navigate and confirm suggestions. Additionally, the `<Tab>` and `<S-Tab>` mappings are cleared to prevent conflicts with other plugins.

```lua
-- Custom CMP mappings
local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
  ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
  ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
  ['<C-y>'] = cmp.mapping.confirm({ select = true }),
  ["<C-Space>"] = cmp.mapping.complete(),
})

cmp_mappings['<Tab>'] = nil
cmp_mappings['<S-Tab>'] = nil
```

The LSP integration with CMP is set up using `lsp.setup_nvim_cmp`.

```lua
lsp.setup_nvim_cmp({
  mapping = cmp_mappings
})
```

## Additional Preferences

Custom preferences are set using `lsp.set_preferences`, including disabling suggestions for LSP servers and defining sign icons for diagnostics.

```lua
lsp.set_preferences({
    suggest_lsp_servers = false,
    sign_icons = {
        error = 'E',
        warn = 'W',
        hint = 'H',
        info = 'I'
    }
})
```

## Key Mappings

Key mappings for common LSP actions are defined using `lsp.on_attach`. These mappings enable quick access to actions like definition, hover, workspace symbol search, and more.

```lua
lsp.on_attach(function(client, bufnr)
  local opts = {buffer = bufnr, remap = false}

  vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
  vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
  -- ... (other mappings) ...
end)
```

## Configuration Setup

The LSP configuration is finalized using `lsp.setup`. This ensures that the LSP settings are applied to your Neovim session.

```lua
lsp.setup()
```

## Diagnostic Configuration

Diagnostics are configured to show virtual text.

```lua
vim.diagnostic.config({
    virtual_text = true
})
```


## License

This project is licensed under the [MIT License](LICENSE).

---
